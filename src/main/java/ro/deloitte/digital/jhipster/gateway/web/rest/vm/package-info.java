/**
 * View Models used by Spring MVC REST controllers.
 */
package ro.deloitte.digital.jhipster.gateway.web.rest.vm;
