import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    ConferenceComponent,
    ConferenceDetailComponent,
    ConferenceUpdateComponent,
    ConferenceDeletePopupComponent,
    ConferenceDeleteDialogComponent,
    conferenceRoute,
    conferencePopupRoute
} from './';

const ENTITY_STATES = [...conferenceRoute, ...conferencePopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ConferenceComponent,
        ConferenceDetailComponent,
        ConferenceUpdateComponent,
        ConferenceDeleteDialogComponent,
        ConferenceDeletePopupComponent
    ],
    entryComponents: [ConferenceComponent, ConferenceUpdateComponent, ConferenceDeleteDialogComponent, ConferenceDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConferencesConferenceModule {}
