import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IConference } from 'app/shared/model/conferences/conference.model';

type EntityResponseType = HttpResponse<IConference>;
type EntityArrayResponseType = HttpResponse<IConference[]>;

@Injectable({ providedIn: 'root' })
export class ConferenceService {
    public resourceUrl = SERVER_API_URL + 'conferences/api/conferences';

    constructor(protected http: HttpClient) {}

    create(conference: IConference): Observable<EntityResponseType> {
        return this.http.post<IConference>(this.resourceUrl, conference, { observe: 'response' });
    }

    update(conference: IConference): Observable<EntityResponseType> {
        return this.http.put<IConference>(this.resourceUrl, conference, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IConference>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IConference[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
