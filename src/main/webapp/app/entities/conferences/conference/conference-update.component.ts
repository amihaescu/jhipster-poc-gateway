import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IConference } from 'app/shared/model/conferences/conference.model';
import { ConferenceService } from './conference.service';

@Component({
    selector: 'jhi-conference-update',
    templateUrl: './conference-update.component.html'
})
export class ConferenceUpdateComponent implements OnInit {
    conference: IConference;
    isSaving: boolean;

    constructor(protected conferenceService: ConferenceService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ conference }) => {
            this.conference = conference;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.conference.id !== undefined) {
            this.subscribeToSaveResponse(this.conferenceService.update(this.conference));
        } else {
            this.subscribeToSaveResponse(this.conferenceService.create(this.conference));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IConference>>) {
        result.subscribe((res: HttpResponse<IConference>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
