export const enum TicketType {
    BLIND = 'BLIND',
    EARLY_BIRD = 'EARLY_BIRD',
    REGULAR = 'REGULAR',
    VIP = 'VIP'
}

export interface ITicket {
    id?: number;
    type?: TicketType;
    price?: number;
}

export class Ticket implements ITicket {
    constructor(public id?: number, public type?: TicketType, public price?: number) {}
}
