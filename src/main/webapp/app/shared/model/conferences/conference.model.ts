export interface IConference {
    id?: number;
    title?: string;
    description?: string;
    technology?: string;
    capacity?: number;
}

export class Conference implements IConference {
    constructor(
        public id?: number,
        public title?: string,
        public description?: string,
        public technology?: string,
        public capacity?: number
    ) {}
}
